import Nav from './Nav';
import AttendeesList from './AttendeesList.js'
import LocationForm from './new-location.js'
import {BrowserRouter, Routes, Route} from 'react-router-dom'
import ConferenceForm from './ConferenceForm';
import AttendConferenceForm from './AttendConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';

function App(props) {

  return (

    <BrowserRouter>
      <Nav />
      <div className="my-5 container">
        <Routes>
          <Route path="locations">
            <Route path="new" element={<LocationForm />} />
          </Route>
          <Route path="conferences">
            <Route path="new" element={<ConferenceForm />} />
          </Route>
          <Route path="attendees">
            <Route index element={<AttendeesList attendees={props.attendees}/>} />
            <Route path="new" element={<AttendConferenceForm />} />
          </Route>
          <Route path="presentation">
            <Route path="new" element={<PresentationForm />} />
          </Route>
          <Route index element={<MainPage />} />
        </Routes>
      </div>
    </BrowserRouter>

  );
}

export default App;
